package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPopUpPage extends ProjectMethods{

	public FindLeadsPopUpPage() {
		PageFactory.initElements(driver, this);
	} 
	
	
	@FindBy(xpath="//input[@name='firstName']")
	WebElement eleFirst;
	public FindLeadsPopUpPage typeFirstName(String data) {
		type(eleFirst, data);  
		return this;
	}

	@FindBy(xpath="//button[text()='Find Leads']")WebElement elefl;
	public FindLeadsPopUpPage clickFindLeadButton() {
		click(elefl);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")WebElement eleOption;
	public MergeLeadPage clickFirstOption() {
		clickWithNoSnap(eleOption);
		switchToWindow(0);
		return new MergeLeadPage();

}
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")WebElement getFirstResultingLead;
	public FindLeadsPopUpPage getFirstResultingLead () {
		//return getText(getFirstResultingLead);
		
		LeadID=getText(getFirstResultingLead);
		return this;

}
	


}




