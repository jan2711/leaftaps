package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	} 
	
	@FindBy(linkText="Create Lead") WebElement eleCreateLead;
	@And("Click CreateLead")

	public CreateLeadPage clickCreateLead() {
	    click(eleCreateLead);  
	    return new 	CreateLeadPage();
	}
	@FindBy(linkText="Find Leads") WebElement eleFindLead;
	@And("Click on FindLeads")
	public FindLeadPage clickFindLeads() {
	    click(eleFindLead);  
	    return new 	FindLeadPage();
	}
	@FindBy(linkText="Merge Leads") WebElement eleMergeLead;
	public MergeLeadPage clickMergeLeads() {
	    click(eleMergeLead);  
	    return new 	MergeLeadPage();
	}
	
	
	
	
}







