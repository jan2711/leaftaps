package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	} 
	
	
	@FindBy(id="viewLead_firstName_sp")
	WebElement eleVerify;
	public ViewLeadPage verifyFirstName(String data) {
	      verifyExactText(eleVerify, data);  
	     return this;
	}
	
	@FindBy(linkText="Edit")
	WebElement eleEditLead;
	@And("Click on Edit")
	public EditLeadPage clickEdit() {
		click(eleEditLead);
		return new EditLeadPage();

}
	@FindBy(id="viewLead_companyName_sp")
	WebElement eleVerifyCn;
	@Then("Verify company name as (.*)")
	public ViewLeadPage verifyCompanyName(String data) {
	      verifyPartialText(eleVerifyCn, data);  
	     return this;
	}
	@FindBy(linkText="Duplicate Lead")
	WebElement eleDupLead;
	public DuplicateLeadPage clickDuplicate() {
		click(eleDupLead);
		return new DuplicateLeadPage();

}
	@FindBy(linkText="Find Leads")
	WebElement eleFLead;
	public FindLeadPage clickF() {
		clickWithNoSnap(eleFLead);
		return new FindLeadPage();

}
	@FindBy(linkText="Delete")
	WebElement eleDelete;
	public MyLeadsPage clickDelete() {
		click(eleDelete);
		return new MyLeadsPage();
	}
}





