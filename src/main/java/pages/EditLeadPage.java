package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	} 
	
	
	@FindBy(xpath="(//input[@name='companyName'])[2]")
	WebElement eleCompan;
	public EditLeadPage clearCompany() {
		eleCompan.clear();
		return  this;
	}
	
	@FindBy(xpath="(//input[@name='companyName'])[2]")
	WebElement eleC;
	@And("Update the companyname as (.*)")

	public EditLeadPage typeCompany(String data) {
		type(eleC, data);
		return this;
}
	@FindBy(xpath="//input[@class='smallSubmit']")
	WebElement eleUpdate;
    @When("Click on Update")

	public ViewLeadPage clickUpdate() {
		click(eleUpdate);
		return new ViewLeadPage();
}

}




