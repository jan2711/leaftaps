package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods{

	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
	} 


	@FindBy(xpath="(//img[@alt='Lookup'])[1]")
	WebElement eleFrom;
	public FindLeadsPopUpPage clickFromLead() {
		clickWithNoSnap(eleFrom);
		switchToWindow(1);
		return  new FindLeadsPopUpPage();
	}
	@FindBy(xpath="(//img[@alt='Lookup'])[2]")
	WebElement eleTo;
	public FindLeadsPopUpPage clickToLead() {
		clickWithNoSnap(eleTo);
		switchToWindow(1);
		return  new FindLeadsPopUpPage();
	}
	@FindBy(className="buttonDangerous")
	WebElement eleMergeButton;
	public ViewLeadPage clickMerge() {
		clickWithNoSnap(eleMergeButton);
		acceptAlert();
		return new ViewLeadPage();
	}
}




