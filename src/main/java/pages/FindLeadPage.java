package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{
	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	} 



	@FindBy(xpath="(//input[@name='firstName'])[3]")
	WebElement eleFirst;
	@And("Enter the firstName as (.*)")

	public FindLeadPage typeFirstName(String data) {
		type(eleFirst, data);  
		return this;
	}


	@FindBy(xpath="//input[@name='id']")
	WebElement eleleadID;
	public FindLeadPage typeLeadID(String data) {
		type(eleleadID, data);  
		return this;
	}



	@FindBy(xpath="//button[text()='Find Leads']")WebElement elefl;
	@And("Click on FindLeadsButton")
	public FindLeadPage clickFindLeadButton() {
		click(elefl);
		return this;
	}

	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")WebElement getFirstResultingLead;
	public FindLeadPage getFirstResultingLead () {
		//return getText(getFirstResultingLead);
		LeadID=getText(getFirstResultingLead);
		return this;

	}

	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")WebElement eleOption;
	@And("Click on FirstLead")
	public ViewLeadPage clickFirstOption() {
		click(eleOption);
		return new ViewLeadPage();
	}
	
	@FindBy(className="x-paging-info")
	WebElement eleText;
	public FindLeadPage verifyText(String data) {
		verifyExactText(eleText, data);
		return new FindLeadPage();
	}

}
/*@FindBy(xpath="//button[text()='Find Leads']")WebElement elefl;
	public FindLeadPage clickFindLeadButton() {
		click(elefl);
		return this;
	}

	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")WebElement eleOption;
	public MergeLeadPage clickFirstOption() {
		click(eleOption);
		switchToWindow(0);
		return new MergeLeadPage();

}
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")WebElement getFirstResultingLead;
	public FindLeadPage getFirstResultingLead () {
		//return getText(getFirstResultingLead);
		LeadID=getText(getFirstResultingLead);
		return this;

}
	@FindBy(className="x-paging-info")
	WebElement eleText;
	public FindLeadPage verifyText(String data) {
		verifyExactText(eleText, data);
		return new FindLeadPage();
	}*/







