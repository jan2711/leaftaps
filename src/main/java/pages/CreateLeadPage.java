package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	} 
	
	
	
	@FindBy(id="createLeadForm_companyName")
	WebElement elecn;

@And("Enter the company name(.*)")
	public CreateLeadPage typeCompanyName(String data) {
	     type(elecn, data);  
	     return this;
	}
	
	@FindBy(id="createLeadForm_firstName")WebElement elefn;

@And("Enter the first name(.*)")
	public CreateLeadPage typeFirstName(String data) {
	     type(elefn, data);  
	     return this;
	}
	
	@FindBy(id="createLeadForm_lastName")WebElement eleln;

@And("Enter the last name(.*)")
	public CreateLeadPage typeLastName(String data) {
	     type(eleln, data);  
	     return this;
	}

	@FindBy(className="smallSubmit")WebElement eleSubmit;
	@When("Click CreateLeadButton")
	public ViewLeadPage clickCreateLead() {
	     click(eleSubmit);  
	     return new ViewLeadPage();
	}
}







