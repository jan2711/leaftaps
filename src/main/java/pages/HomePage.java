package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	} 
	
	@FindBy(className="decorativeSubmit") WebElement eleLogOut;

	public LoginPage clickLogOut() {
		//WebElement eleLogOut = locateElement("class", "decorativeSubmit");
	    click(eleLogOut);  
	    return new LoginPage();
	    
	}
	
	@FindBy(linkText="CRM/SFA") WebElement eleCRMSFA;
	@And("Click CRMSFA")
	public MyHomePage clickCRMSFA() {
		//WebElement eleCRMSFA = locateElement("linktext", "CRM/SFA");
	    click(eleCRMSFA);  
	    return new MyHomePage();
	}
}







