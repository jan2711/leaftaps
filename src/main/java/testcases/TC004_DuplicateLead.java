package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DuplicateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_DuplicateLead";
		testDescription = "Duplicate the Lead";
		testNodes = "Leads";
		authors ="Janani";
		category="Regression";
		dataSheetName = "TC004";
	}

	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd,String fname) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.typeFirstName(fname)
		.clickFindLeadButton()
		.clickFirstOption()
		.clickDuplicate()
		.clickCreateLead()
		.verifyFirstName(fname);
		
		
	}
}









