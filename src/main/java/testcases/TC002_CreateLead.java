package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "LeadCreation";
		testNodes = "Leads";
		authors ="Janani";
		category="sanity";
		dataSheetName = "TC002";
	}

	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd,String cname,String fname,String lname ) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.typeCompanyName(cname)
		.typeFirstName(fname)
		.typeLastName(lname)
		.clickCreateLead()
		.verifyFirstName(fname);

	}
}









