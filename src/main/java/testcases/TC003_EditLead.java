package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_EditLead";
		testDescription = "Edit the Lead";
		testNodes = "Leads";
		authors ="Janani";
		category="sanity";
		dataSheetName = "TC003";
	}

	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd,String fname,String cname ) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.typeFirstName(fname)
		.clickFindLeadButton()
		.clickFirstOption()
		.clickEdit()
		.typeCompany(cname)
		.clickUpdate()
		.verifyCompanyName(cname);

	}
}









