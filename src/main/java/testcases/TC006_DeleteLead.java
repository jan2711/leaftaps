package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC006_DeleteLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC006_DeleteLead";
		testDescription = "Delete the Lead";
		testNodes = "Leads";
		authors ="Janani";
		category="regression";
		dataSheetName = "TC006";
	}

	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd,String fname,String text) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.typeFirstName(fname)
		.clickFindLeadButton()
		.getFirstResultingLead()
		.clickFirstOption()
		.clickDelete()
		.clickFindLeads()
		.typeLeadID(LeadID)
		.clickFindLeadButton()
		.verifyText(text);
	}
}









