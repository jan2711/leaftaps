package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.FindLeadPage;
import pages.FindLeadsPopUpPage;
import pages.LoginPage;
import pages.MergeLeadPage;
import pages.ViewLeadPage;
import wdMethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC005_MergeLead";
		testDescription = "Merge the Lead";
		testNodes = "Leads";
		authors ="Janani";
		category="Regression";
		dataSheetName = "TC005";
	}

	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd,String from,String to,String text) {
		 new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickMergeLeads()
		.clickFromLead()
		.typeFirstName(from)
		.clickFindLeadButton()
		.getFirstResultingLead()
		.clickFirstOption()
		.clickToLead()
		.typeFirstName(to)
		.clickFindLeadButton()
		.clickFirstOption()
		.clickMerge()
		.clickF()
		.typeLeadID(LeadID)
		.clickFindLeadButton()
		.verifyText(text);

		
	}
}









