/*package steps2;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	ChromeDriver driver;

@Given("Launch the browser")
public void launchTheBrowser() {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
    driver = new ChromeDriver();
}

@Given("Load the URL")
public void loadTheURL() {
    driver.get("http://leaftaps.com/opentaps/control/main");
}

@Given("Maximize the browser")
public void maximizeTheBrowser() {
  driver.manage().window().maximize();
}

@Given("Set the timeout")
public void setTheTimeOut() {
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

}

@Given("Enter username")
public void enterUsernameAsDemoSalesManager() {
 driver.findElementById("username").sendKeys("DemoSalesManager");
}

@Given("Enter password")
public void enterPasswordAsCrmsfa() {
	 driver.findElementById("password").sendKeys("crmsfa");

}

@Given("Click on Loginbutton")
public void clickOnLoginbutton() {
   driver.findElementByClassName("decorativeSubmit").click();
}

@Given("Click on CRMSFA")
public void clickOnCRMSFA() {
   driver.findElementByLinkText("CRM/SFA").click();
}


@Given("Click on CreateLead")
public void clickOnCreateLead() {
   driver.findElementByLinkText("Create Lead").click();
}
@Given("Enter the company name")
public void enterTheCompanyName() {
 driver.findElementById("createLeadForm_companyName").sendKeys("cts");   
}

@Given("Enter the first name")
public void enterTheFirstName() {
	 driver.findElementById("createLeadForm_firstName").sendKeys("Janani");   

}

@Given("Enter the last name")
public void enterTheLastName() {
	 driver.findElementById("createLeadForm_lastName").sendKeys("Bhuvanesh");   

}

@When("Click on CreateLeadButton")
public void clickOnCreateLeadButton() {
    driver.findElementByClassName("smallSubmit").click();
}

@Then("Verify the title")
public void verifyTheTitleViewLead() {
String text = driver.getTitle();
System.out.println(text);
if(text.contains("View Lead")) {
	System.out.println("Lead created");
}
else
{	System.out.println("Lead not created");
}
	
}
}
	
*/