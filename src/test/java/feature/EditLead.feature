Feature: Create Lead into Leaftaps
#Background:
#    Given Launch browser 
#	And Load URL
#	And Maximize browser 
#	And Set timeout 
@smoke
Scenario Outline: Create Lead
And Enter username as <username>
	And Enter password as <pass> 
	And Click Loginbutton
	And Click CRMSFA 
	And Click CreateLead
And Enter the company name<cname>
And Enter the first name<fname>
And Enter the last name<lname>
When Click CreateLeadButton

Examples: 
		|username|pass|cname|fname|lname|title|
		|DemoSalesManager|crmsfa|Cognizant|Janani|Bhuvanesh|View Lead|
@sanity @regression
Scenario Outline: Edit Lead
And Enter username as <username>
	And Enter password as <pass> 
	And Click Loginbutton
	And Click CRMSFA 
And Click on Leadsbutton
	And Click on FindLeads
	And Enter the firstName as <finame>
	And Click on FindLeadsButton
	And Click on FirstLead
	#Then Verify the title View Lead
	And Click on Edit 
    #Then Verify the title Edit Lead<etitle>
    And Update the companyname as <cname>
    When Click on Update
    Then Verify company name as <cname> 
 
		
Examples: 
		|username|pass|finame|cname|cname|
		|DemoSalesManager|crmsfa|Janani|Cognizant|Cognizant|
		
		
#		Scenario Outline: Merge Lead
#And Enter username as <username>
#	And Enter password as <pass> 
#	And Click Loginbutton
#	And Click CRMSFA 
#And Click on Leadsbutton
#	And Click on MergeLeads
#	And Click on FromLeadsButton
#	And Enter the firstnamefield
#	And Click on FindLeadsButton
#	And Click on FirstLead
#	And Click on ToLeadsButton
#	And Enter the firstnamefield
#	And Click on FindLeadsButton
#	And Click on FirstLead
#	And Click on Merge
#	And Click on 
#	
#	
#	
#	
#		
#Examples: 
#		|username|pass|finame|cname|cname|
#		|DemoSalesManager|crmsfa|Janani|Cognizant|Cognizant|