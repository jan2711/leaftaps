package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/feature/CreateLead.feature",glue="steps2",dryRun=false,
snippets=SnippetType.CAMELCASE,monochrome=true,plugin = {"pretty","html:CucumberReports"})


public class RunTest {
	

}
