/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EditLead {
	ChromeDriver driver;

@Given("Launch browser")
public void launchBrowser() {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
    driver = new ChromeDriver();
}

@Given("Load URL")
public void loadURL() {
    driver.get("http://leaftaps.com/opentaps/control/main");

}

@Given("Maximize browser")
public void maximizeBrowser() {
	  driver.manage().window().maximize();

}

@Given("Set timeout")
public void setTimeout() { 
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}

@Given("Enter username as (.*)")
public void enterUsernameAsUsername(String uname) {
	 driver.findElementById("username").sendKeys(uname);
}

@Given("Enter password as (.*)")
public void enterPasswordAsPass(String pass) {
	 driver.findElementById("password").sendKeys(pass);

}

@Given("Click Loginbutton")
public void clickLoginbutton() {
	   driver.findElementByClassName("decorativeSubmit").click();

}

@Given("Click CRMSFA")
public void clickOnCRMSFA() {
	   driver.findElementByLinkText("CRM/SFA").click();

}


@Given("Click CreateLead")
public void clickOnCreateLead() {
   driver.findElementByLinkText("Create Lead").click();
}
@Given("Enter the company name(.*)")
public void enterTheCompanyName(String company) {
 driver.findElementById("createLeadForm_companyName").sendKeys(company);   
}

@Given("Enter the first name(.*)")
public void enterTheFirstName(String fname) {
	 driver.findElementById("createLeadForm_firstName").sendKeys(fname);   

}

@Given("Enter the last name(.*)")
public void enterTheLastName(String lname) {
	 driver.findElementById("createLeadForm_lastName").sendKeys(lname);   

}

@When("Click CreateLeadButton")
public void clickOnCreateLeadButton() {
    driver.findElementByClassName("smallSubmit").click();
}

@Given("Click on Leadsbutton")
public void clickOnLeadsbutton() {
	   driver.findElementByLinkText("Leads").click();

}
@Given("Click on FindLeads")
public void clickOnFindLeads() {
driver.findElementByLinkText("Find Leads").click();
}

@Given("Enter the firstName as (.*)")
public void enterTheFirstNameAsFname(String fn) {
	driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fn);
			
	
}


@Given("Click on FindLeadsButton")
public void clickOnFindLeadsButton() throws InterruptedException {
    // Write code here that turns the phrase above into concrete actions
driver.findElementByXPath("//button[text()='Find Leads']").click();
Thread.sleep(3000);
		}

@When("Click on FirstLead")
public void clickOnFirsLead() throws InterruptedException {
	driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
	Thread.sleep(3000);
}

@Then("Verify the title View Lead(.*)")
public void verifyTheTitleViewLead(String title) {
//title = driver.getTitle();
//System.out.println(title);
if(title.contains("View Lead")) {
	System.out.println("Lead created");
}
else
{	System.out.println("Lead not created");
}
	
}
	

@When("Click on Edit")
public void clickOnEdit() throws InterruptedException {
  driver.findElementByLinkText("Edit").click();
  Thread.sleep(3000);
}

@Then("Verify the title Edit Lead(.*)")
public void verifyTheTitleEditLead(String etitle) {
	if(etitle.contains("Edit Lead")) {
		System.out.println("Lead created");
	}
	else
	{	System.out.println("Lead not created");
	}
		
	}


@Then("Update the companyname as (.*)")
public void updateTheCompanynameAsCname(String cn) {
driver.findElementByXPath("(//input[@name='companyName'])[2]").clear();
driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys(cn);

}

@When("Click on Update")
public void clickOnUpdate() {
    driver.findElementByXPath("//input[@class='smallSubmit']").click();
}

@Then("Verify company name as (.*)")
public void verifyCompanyNameAsCname(String co) {
    
	if(co.contains("Cognizant")) {
		System.out.println("Lead edited");
	}
	else
	{	System.out.println("Lead not edited");
	}
		
	
}



	
	
}


*/